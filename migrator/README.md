# Migration Tool
Migration tool is essentially a translator from Java and Kotlin to StaticTS, 
a TypeScript-based language whose specification is currently under development.
At the moment, migration tool is in the early stages of development.

## Building

### Requirements
This project is using the **Apache Ant** tool for building. You can download binaries from [here](https://ant.apache.org/bindownload.cgi) (it's recommended to use the latest available version) and use the [official guide](https://ant.apache.org/manual/install.html) to install and setup the tool.

You also need to use **Java 8** (or newer) to build the project.

### Steps to build

The build supports two main targets: **clean** and **build**:
- Use **ant clean build** to perform build with preliminary cleaning of previous build artifacts. 
- Use **ant build** to do incremental build (does not re-build sources that didn't change).

The result jar file is **out/migrator-[version].jar**.

## Running
To use migration tool after the build, run the following command from the top-level folder of 
the repository:

java -jar out/migrator-[version].jar [options] [input files]

To get the list of available command-line options, run:

java -jar out/migrator-[version].jar -help

## Running tests
At the moment, only tests for migration from Java can be run. To do so, run **ant test_java** 
on the command line. Resulting STS files are written to test/java/results folder and compared
to expected STS files in test/java folder.
