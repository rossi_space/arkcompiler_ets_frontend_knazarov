/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.migrator.java;

import com.ohos.migrator.AbstractTranspiler;

import com.ohos.migrator.ResultCode;
import com.ohos.migrator.TranspileException;

import com.ohos.migrator.staticTS.parser.StaticTSParser.*;

import org.eclipse.jdt.core.dom.CompilationUnit;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Java to StaticTS transpiler class inherited from AbstractTranspiler class.
 * Transpilation consists of 3 steps:
 * 1) Parse Java source file and create Java AST.
 * 2) Translate Java AST to StaticTS AST.
 * 3) Write output file with StaticTS source code.
*/

public class JavaTranspiler extends AbstractTranspiler {

    private boolean noxrefs = false;
    public JavaTranspiler(List<File> src, List<File> libs, String outDir, boolean noxrefs) {
       super(src, libs, outDir);
       this.noxrefs = noxrefs;
    }

    @Override
    public void transpileFile(File srcFile) throws TranspileException {
        try {
            CompilationUnit javaCU = parse(srcFile);

            CompilationUnitContext stsCU = transform(javaCU, srcFile);

            write(stsCU, srcFile);
        } catch (IOException e) {
            throw new TranspileException(ResultCode.InputError, e);
        } catch (JavaParserException e) {
            throw new TranspileException(ResultCode.ParseError, e);
        }
    }

    private CompilationUnit parse(File srcFile) throws IOException, JavaParserException {
        JavaParser parser = new JavaParser(srcFile, sourceFiles, libFiles, noxrefs);
        return parser.parse();
    }

    private CompilationUnitContext transform(CompilationUnit javaCU, File srcFile) {
        JavaTransformer transformer = new JavaTransformer(javaCU, srcFile);
        return transformer.transform();
    }

    @Override
    public double getConversionRate() {
        return JavaTransformer.getTransformationRate() * 100.;
    }
}