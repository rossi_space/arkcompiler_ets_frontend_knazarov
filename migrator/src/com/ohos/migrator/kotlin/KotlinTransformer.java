/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.migrator.kotlin;

import com.ohos.migrator.Transformer;
import com.ohos.migrator.staticTS.parser.StaticTSParser.*;

import org.antlr.v4.runtime.ParserRuleContext;
import org.jetbrains.kotlin.analyzer.AnalysisResult;
import org.jetbrains.kotlin.psi.KtFile;
import org.jetbrains.kotlin.psi.KtVisitor;
import org.jetbrains.kotlin.resolve.BindingContext;

/**
 * Performs transformation of the Kotlin AST into StaticTS AST.
 */
public class KotlinTransformer extends KtVisitor<ParserRuleContext, Void> implements Transformer {
    KtFile ktFile;
    AnalysisResult analysisResult;
    BindingContext bindingContext;

    public KotlinTransformer(KtFile ktFile, AnalysisResult analysisResult) {
        this.ktFile = ktFile;
        this.analysisResult = analysisResult;
        this.bindingContext = analysisResult.getBindingContext();
    }

    public CompilationUnitContext transform() {
        return (CompilationUnitContext)visitKtFile(ktFile, null);
    }

    @Override
    public ParserRuleContext visitKtFile(KtFile node, Void data) {
        return new CompilationUnitContext(null, 0);
    }
}