/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 by Bart Kiers (original author) and Alexandre Vitorelli (contributor -> ported to CSharp)
 * Copyright (c) 2017 by Ivan Kochurkin (Positive Technologies):
    added ECMAScript 6 support, cleared and transformed to the universal grammar.
 * Copyright (c) 2018 by Juan Alvarez (contributor -> ported to Go)
 * Copyright (c) 2019 by Andrii Artiushok (contributor -> added TypeScript support)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
parser grammar StaticTSParser;

options {
    tokenVocab=StaticTSLexer;
    superClass=StaticTSParserBase;
}

compilationUnit
    : packageDeclaration? importDeclaration* topDeclaration* EOF
    ;

packageDeclaration
    : Package qualifiedName SemiColon? // notLineTerminator
    ;

importDeclaration
    : Import qualifiedName ((As Identifier) | (Dot Multiply))? SemiColon? // notLineTerminator
    ;

qualifiedName
    : Identifier (Dot Identifier)*
    ;

topDeclaration
    : Export?
    (
          classDeclaration
        | interfaceDeclaration
        | enumDeclaration
        | functionDeclaration
        | variableOrConstantDeclaration
    )
    ;

// Classes
classDeclaration
    : (Static? (Abstract | Open) | (Abstract | Open)? Static)?
      Class Identifier typeParameters? classExtendsClause? implementsClause? classBody
    ;

accessibilityModifier
    : Public
    | Private
    | Protected
    ;

classExtendsClause
    : Extends typeReference
    ;

implementsClause
    : Implements interfaceTypeList
    ;

classBody
    :  OpenBrace classMember* clinit=classInitializer? classMember* CloseBrace
    ;

classMember
    : accessibilityModifier?
    (
          constructorDeclaration
        | classFieldDeclaration
        | classMethodDeclaration
        | interfaceDeclaration
        | enumDeclaration
        | classDeclaration
    )
    ;

constructorDeclaration
    : Constructor typeParameters? OpenParen parameterList? CloseParen constructorBody
    ;

parameterList
    : parameter (Comma parameter)* (Comma variadicParameter)?
    | variadicParameter
    ;

parameter
    : Identifier typeAnnotation
    ;

variadicParameter
    : Ellipsis Identifier typeAnnotation
    ;

typeAnnotation // TODO: Rename to typeSpecifier
    : Colon primaryType
    ;

constructorBody
    : OpenBrace constructorCall? statementOrLocalDeclaration* CloseBrace
    ;

constructorCall
    : Try?
    (
      This typeArguments? arguments
      | (singleExpression Dot)? Super typeArguments? arguments
    )
    ;

statementOrLocalDeclaration
    : statement
    | variableOrConstantDeclaration
    | interfaceDeclaration
    | classDeclaration
    | enumDeclaration
    ;

classFieldDeclaration
    : Static? (variableDeclaration | Const constantDeclaration) SemiColon
    | Const Static? constantDeclaration SemiColon
    ;

initializer
    : Assign singleExpression
    ;

classMethodDeclaration
    : (Static | Override | Open)? Identifier signature block                        #ClassMethodWithBody
    | (Abstract | Static? Native | Native Static)? Identifier signature SemiColon   #AbstractOrNativeClassMethod
    ;

classInitializer
    : Static block
    ;

signature
    : typeParameters? OpenParen parameterList? CloseParen typeAnnotation
    ;

// Interfaces
interfaceDeclaration
    : Static? Interface Identifier typeParameters? interfaceExtendsClause? OpenBrace interfaceBody CloseBrace
    ;

interfaceExtendsClause
    : Extends interfaceTypeList
    ;

interfaceTypeList
    : typeReference (Comma typeReference)*
    ;

interfaceBody
    : interfaceMember*
    ;

interfaceMember
    : Identifier signature SemiColon                  #InterfaceMethod
    | (Static | Private)? Identifier signature block  #InterfaceMethodWithBody
    | constantDeclaration SemiColon                   #InterfaceField
    | interfaceDeclaration                            #InterfaceInInterface
    | classDeclaration                                #ClassInInterface
    | enumDeclaration                                 #EnumInInterface
    ;

// Enums
enumDeclaration
    : Enum Identifier OpenBrace enumBody? CloseBrace
    ;

enumBody
    : enumMember (Comma enumMember)*
    ;

enumMember
    : Identifier (Assign singleExpression)?
    ;

// Functions
functionDeclaration
    : Function Identifier signature block
    ;

// Variables & constants
variableOrConstantDeclaration
    : ((Let variableDeclarationList) | (Const constantDeclarationList)) SemiColon
    ;

variableDeclarationList
    : variableDeclaration (Comma variableDeclaration)*
    ;

constantDeclarationList
    : constantDeclaration (Comma constantDeclaration)*
    ;

variableDeclaration
    : Identifier typeAnnotation initializer?
    | Identifier initializer
    ;

constantDeclaration
    : Identifier typeAnnotation? initializer
    ;

// Types
intersectionType
    : OpenParen typeReference (BitAnd typeReference)+ CloseParen
    ;

primaryType
    : predefinedType
    | typeReference
    | arrayType
    ;

predefinedType
    : Byte
    | Short
    | Int
    | Long
    | Float
    | Double
    | Boolean
    | String
    | Char
    | Void
    ;

arrayType
    : (predefinedType | typeReference) {notLineTerminator()}? (OpenBracket CloseBracket)+
    ;

typeReference
    : typeReferencePart (Dot typeReferencePart)*
    ;

typeReferencePart
    : qualifiedName typeArguments?
    ;

// Generics
typeParameters
    : LessThan typeParameterList MoreThan
    ;

typeParameterList
    : typeParameter (Comma typeParameter)*
    ;

typeParameter
    : Identifier constraint?
    ;

constraint
    : Extends (typeReference | intersectionType)
    ;

typeArguments
    : LessThan typeArgumentList? MoreThan
    ;

typeArgumentList
    : typeArgument (Comma typeArgument)*
    ;

typeArgument
    : typeReference
    | arrayType
    | wildcardType
    ;

wildcardType
    : QuestionMark wildcardBound?
    ;

wildcardBound
    : (Extends | Super) typeReference
    ;

// Statements
statement
    : block
    | assertStatement
    | ifStatement
    | iterationStatement
    | continueStatement
    | breakStatement
    | returnStatement
    | labelledStatement
    | switchStatement
    | throwStatement
    | deferStatement
    | trapStatement
    | expressionStatement
    ;

block
    : OpenBrace statementOrLocalDeclaration* CloseBrace
    ;

assertStatement
    : Assert condition=singleExpression (Colon message=singleExpression)? SemiColon
    ;

ifStatement
    : If OpenParen singleExpression CloseParen ifStmt=statement (Else elseStmt=statement)?
    ;

iterationStatement
    : Do statement While OpenParen singleExpression CloseParen SemiColon                                     # DoStatement
    | While OpenParen singleExpression CloseParen statement                                                  # WhileStatement
    | For OpenParen forInit? SemiColon singleExpression? SemiColon expressionSequence? CloseParen statement  # ForStatement
    | For OpenParen Let Identifier typeAnnotation? Of singleExpression CloseParen statement                  # ForOfStatement
    ;

forInit
    : expressionSequence | Let variableDeclarationList
    ;

continueStatement
    : Continue ({this.notLineTerminator()}? Identifier)? SemiColon
    ;

breakStatement
    : Break ({this.notLineTerminator()}? Identifier)? SemiColon
    ;

returnStatement
    : Return ({this.notLineTerminator()}? singleExpression)? SemiColon
    ;

labelledStatement
    : Identifier Colon statement
    ;

switchStatement
    : Switch OpenParen singleExpression CloseParen caseBlock
    ;

caseBlock
    : OpenBrace leftCases=caseClauses? defaultClause? rightCases=caseClauses? CloseBrace
    ;

caseClauses
    : caseClause+
    ;

caseClause
    : Case singleExpression ':' statement*
    ;

defaultClause
    : Default ':' statement*
    ;

throwStatement
    : Throw {this.notLineTerminator()}? singleExpression SemiColon
    ;

trapStatement
    : Trap block catchOrRecoverClause+
    ;

catchOrRecoverClause
    : (Catch | Recover) exceptionParameter? block
    ;

exceptionParameter
    : OpenParen Identifier typeAnnotation CloseParen
    ;

deferStatement
    : Defer statement
    ;

expressionStatement
    : {this.notOpenBraceAndNotFunction()}? singleExpression SemiColon?
    ;

// Expressions
singleExpression
    : OpenParen parameterList? CloseParen typeAnnotation Arrow lambdaBody    # LambdaExpression
    | singleExpression indexExpression                                       # ArrayAccessExpression
    | singleExpression Dot Identifier                                        # MemberAccessExpression
    | New typeArguments? (singleExpression Dot)? typeReference arguments? classBody?   # NewClassExpression
    | New primaryType indexExpression+ (OpenBracket CloseBracket)*           # NewArrayExpression
    | singleExpression typeArguments? arguments                              # CallExpression
    | singleExpression {this.notLineTerminator()}? PlusPlus                  # PostIncrementExpression
    | singleExpression {this.notLineTerminator()}? MinusMinus                # PostDecreaseExpression
    | PlusPlus singleExpression                                              # PreIncrementExpression
    | MinusMinus singleExpression                                            # PreDecreaseExpression
    | Plus singleExpression                                                  # UnaryPlusExpression
    | Minus singleExpression                                                 # UnaryMinusExpression
    | BitNot singleExpression                                                # BitNotExpression
    | Not singleExpression                                                   # NotExpression
    | singleExpression (Multiply | Divide | Modulus) singleExpression        # MultiplicativeExpression
    | singleExpression (Plus | Minus) singleExpression                       # AdditiveExpression
    | singleExpression shiftOperator singleExpression                        # BitShiftExpression
    | singleExpression (LessThan | MoreThan | LessThanEquals | GreaterThanEquals) singleExpression        # RelationalExpression
    | singleExpression Instanceof primaryType                                # InstanceofExpression
    | singleExpression (Equals | NotEquals) singleExpression                 # EqualityExpression
    | singleExpression BitAnd singleExpression                               # BitAndExpression
    | singleExpression BitXor singleExpression                               # BitXOrExpression
    | singleExpression BitOr singleExpression                                # BitOrExpression
    | singleExpression And singleExpression                                  # LogicalAndExpression
    | singleExpression Or singleExpression                                   # LogicalOrExpression
    | singleExpression QuestionMark singleExpression Colon singleExpression  # TernaryExpression
    | singleExpression Assign singleExpression                               # AssignmentExpression
    | singleExpression assignmentOperator singleExpression                   # AssignmentOperatorExpression
    | (typeReference Dot)? This                                              # ThisExpression
    | Identifier                                                             # IdentifierExpression
    | (typeReference Dot)? Super                                             # SuperExpression
    | literal                                                                # LiteralExpression
    | OpenBracket expressionSequence? CloseBracket                           # ArrayLiteralExpression
    | primaryType Dot Class                                                  # ClassLiteralExpression
    | OpenParen singleExpression CloseParen                                  # ParenthesizedExpression
    | singleExpression As (intersectionType | primaryType)                   # CastExpression
    | Try singleExpression                                                   # TryExpression
    ;

shiftOperator
    : first=LessThan second=LessThan {$first.index + 1 == $second.index}?
    | first=MoreThan second=MoreThan {$first.index + 1 == $second.index}?
    | first=MoreThan second=MoreThan third=MoreThan {$first.index + 1 == $second.index && $second.index + 1 == $third.index}?
    ;

lambdaBody
    : singleExpression
    | block
    ;

arguments
    : OpenParen expressionSequence? CloseParen
    ;

expressionSequence
    : singleExpression (Comma singleExpression)*
    ;

indexExpression
    : OpenBracket singleExpression CloseBracket
    ;

assignmentOperator
    : MultiplyAssign
    | DivideAssign
    | ModulusAssign
    | PlusAssign
    | MinusAssign
    | LeftShiftArithmeticAssign
    | RightShiftArithmeticAssign
    | RightShiftLogicalAssign
    | BitAndAssign
    | BitXorAssign
    | BitOrAssign
    ;

literal
    : NullLiteral
    | BooleanLiteral
    | StringLiteral
    | CharLiteral
    | numericLiteral
    ;

numericLiteral
    : DecimalLiteral
    | HexIntegerLiteral
    | OctalIntegerLiteral
    | BinaryIntegerLiteral
    ;
