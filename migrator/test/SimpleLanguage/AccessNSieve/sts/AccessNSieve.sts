/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// modified by Isaac Gouy

function AccessNSieve() {
  this.n1 = 3;
  this.n2 = 10000;
}

AccessNSieve.prototype.nsieve = function(m, isPrime) {
  let i: int, k: int, count: int;

  for (i = 2; i <= m; i++) {
    isPrime[i] = true;
  }
  count = 0;

  for (i = 2; i <= m; i++) {
    if (isPrime[i]) {
      for (k = i + i; k <= m; k += i) isPrime[k] = false;
      count++;
    }
  }
  return count;
};

AccessNSieve.prototype.sieve = function() {
  let sum: int = 0;
  for (let i: int = 1; i <= this.n1; i++) {
    let m: int = (1 << i) * this.n2;
    let flags: int[] = Array(m + 1);
    sum += this.nsieve(m, flags);
  }
  return sum;
};

AccessNSieve.prototype.pad = function(int, width) {
  let s: string = int.toString();
  let prefixWidth: int = width - s.length;
  if (prefixWidth > 0) {
    for (let i: int = 1; i <= prefixWidth; i++) s = ' ' + s;
  }
  return s;
};

AccessNSieve.prototype.setup = function() {};

AccessNSieve.prototype.run = function() {
  let expected: int = 14302;

  let result: int = this.sieve();

  if (result != expected)
    throw 'ERROR: bad result: expected ' + expected + ' but got ' + result;

  let consumer = new consumer();
  consumer.consumeInt(result);
};
