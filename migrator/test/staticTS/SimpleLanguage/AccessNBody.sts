/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import utils.Consumer;

export class AccessNBody {
  static const PI:            double = 3.141592653589793;
  static const SOLAR_MASS:    double = 4 * PI * PI;
  static const DAYS_PER_YEAR: double = 365.24;
  n1: int = 3;
  n2: int = 24;
  static const expected: double = -1.3524862408537381;

  class Body {
    x: double;
    y: double;
    z: double;
    vx: double;
    vy: double;
    vz: double;
    mass: double;

    constructor(
        x: double,        y: double,
        z: double,
        vx: double,
        vy: double,
        vz: double,
        mass: double) {
      this.x = x;
      this.y = y;
      this.z = z;
      this.vx = vx;
      this.vy = vy;
      this.vz = vz;
      this.mass = mass;
    }

    public offsetMomentum(px: double, py: double, pz: double): Body {
      this.vx = -px / SOLAR_MASS;
      this.vy = -py / SOLAR_MASS;
      this.vz = -pz / SOLAR_MASS;
      return this;
    }
  }

  class NBodySystem {
    public bodies: Body[];

    constructor(bodies: Body[]) {      this.bodies = bodies;
      let px: double = 0.0;
      let py: double = 0.0;
      let pz: double = 0.0;
      let size: int = this.bodies.length;
      for (let i: int = 0; i < size; i++) {
        let b: Body = this.bodies[i];
        let m: double = b.mass;
        px += b.vx * m;
        py += b.vy * m;
        pz += b.vz * m;
      }

      this.bodies[0].offsetMomentum(px, py, pz);
    }

    public advance(dt: double): void {
      let dx: double;
      let dy: double;
      let dz: double;      let distance: double;
      let mag: double;
      let size: int = this.bodies.length;

      for (let i: int = 0; i < size; i++) {
        let bodyi: Body = this.bodies[i];
        for (let j: int = i + 1; j < size; j++) {
          let bodyj: Body = this.bodies[j];
          dx = bodyi.x - bodyj.x;
          dy = bodyi.y - bodyj.y;
          dz = bodyi.z - bodyj.z;

          distance = Math.sqrt(dx * dx + dy * dy + dz * dz);
          mag = dt / (distance * distance * distance);

          bodyi.vx -= dx * bodyj.mass * mag;
          bodyi.vy -= dy * bodyj.mass * mag;
          bodyi.vz -= dz * bodyj.mass * mag;

          bodyj.vx += dx * bodyi.mass * mag;
          bodyj.vy += dy * bodyi.mass * mag;
          bodyj.vz += dz * bodyi.mass * mag;
        }
      }

      for (let i: int = 0; i < size; i++) {
        let body: Body  = this.bodies[i];
        body.x += dt * body.vx;
        body.y += dt * body.vy;
        body.z += dt * body.vz;
      }
    }

    public energy(): double {
      let dx: double;
      let dy: double;
      let dz: double;
      let distance: double;
      let e: double = 0.0;
      let size: int = this.bodies.length;

      for (let i: int = 0; i < size; i++) {
        let bodyi: Body = this.bodies[i];

        e += 0.5 * bodyi.mass * (bodyi.vx * bodyi.vx + bodyi.vy * bodyi.vy + bodyi.vz * bodyi.vz);

        for (let j: int = i + 1; j < size; j++) {
          let bodyj: Body  = this.bodies[j];
          dx = bodyi.x - bodyj.x;
          dy = bodyi.y - bodyj.y;
          dz = bodyi.z - bodyj.z;

          distance = Math.sqrt(dx * dx + dy * dy + dz * dz);
          e -= (bodyi.mass * bodyj.mass) / distance;
        }
      }

      return e;
    }
  }

  private static jupiter(): Body {
    return new Body(
        4.84143144246472090e+0,
        -1.16032004402742839e+0,
        -1.03622044471123109e-1,
        1.66007664274403694e-3 * DAYS_PER_YEAR,
        7.69901118419740425e-3 * DAYS_PER_YEAR,
        -6.90460016972063023e-5 * DAYS_PER_YEAR,
        9.54791938424326609e-4 * SOLAR_MASS
    );
  }

  private static saturn(): Body {
    return new Body(
        8.34336671824457987e+0,
        4.12479856412430479e+0,
        -4.03523417114321381e-1,
        -2.76742510726862411e-3 * DAYS_PER_YEAR,
        4.99852801234917238e-3 * DAYS_PER_YEAR,
        2.30417297573763929e-5 * DAYS_PER_YEAR,
        2.85885980666130812e-4 * SOLAR_MASS
    );
  }

  private static uranus(): Body {
    return new Body(
        1.28943695621391310e+1,
        -1.51111514016986312e+1,
        -2.23307578892655734e-1,
        2.96460137564761618e-3 * DAYS_PER_YEAR,
        2.37847173959480950e-3 * DAYS_PER_YEAR,
        -2.96589568540237556e-5 * DAYS_PER_YEAR,
        4.36624404335156298e-5 * SOLAR_MASS
    );
  }

  private static neptune(): Body {
    return new Body(
        1.53796971148509165e+1,
        -2.59193146099879641e+1,
        1.79258772950371181e-1,
        2.68067772490389322e-3 * DAYS_PER_YEAR,
        1.62824170038242295e-3 * DAYS_PER_YEAR,
        -9.51592254519715870e-5 * DAYS_PER_YEAR,
        5.15138902046611451e-5 * SOLAR_MASS
    );
  }

  private static sun(): Body {
    return new Body(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, SOLAR_MASS);
  }

  public run(): void {
    let ret: double = 0.0;
    for (let n: int = this.n1; n <= this.n2; n *= 2) {
      let bodies: NBodySystem = new NBodySystem(
    //      [sun(), jupiter(), saturn(), uranus(), neptune()]
      );
      let max: int = n * 100;
      ret += bodies.energy();
      for (let i: int = 0; i < max; i++) {
        bodies.advance(0.01);
      }
      ret += bodies.energy();
    }
    if (ret != this.expected) {
      //System.err.println("ERROR: bad result: expected " + expected + " but got " + ret);
      //System.exit(-1);
    }
    Consumer.consumeDouble(ret);
  }

}

