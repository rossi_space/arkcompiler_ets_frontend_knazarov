{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "func",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSCallSignatureDeclaration",
                  "params": [
                    {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 17
                          },
                          "end": {
                            "line": 1,
                            "column": 23
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 14
                        },
                        "end": {
                          "line": 1,
                          "column": 15
                        }
                      }
                    },
                    {
                      "type": "Identifier",
                      "name": "b",
                      "typeAnnotation": {
                        "type": "TSStringKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 28
                          },
                          "end": {
                            "line": 1,
                            "column": 34
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 25
                        },
                        "end": {
                          "line": 1,
                          "column": 26
                        }
                      }
                    }
                  ],
                  "returnType": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 37
                      },
                      "end": {
                        "line": 1,
                        "column": 43
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 13
                    },
                    "end": {
                      "line": 1,
                      "column": 44
                    }
                  }
                },
                {
                  "type": "TSCallSignatureDeclaration",
                  "params": [
                    {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "TSStringKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 49
                          },
                          "end": {
                            "line": 1,
                            "column": 55
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 46
                        },
                        "end": {
                          "line": 1,
                          "column": 47
                        }
                      }
                    },
                    {
                      "type": "Identifier",
                      "name": "b",
                      "typeAnnotation": {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 61
                          },
                          "end": {
                            "line": 1,
                            "column": 67
                          }
                        }
                      },
                      "optional": true,
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 57
                        },
                        "end": {
                          "line": 1,
                          "column": 58
                        }
                      }
                    }
                  ],
                  "returnType": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 70
                      },
                      "end": {
                        "line": 1,
                        "column": 76
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 45
                    },
                    "end": {
                      "line": 1,
                      "column": 78
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 11
                },
                "end": {
                  "line": 1,
                  "column": 78
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 9
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 9
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 79
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 2,
              "column": 1
            },
            "end": {
              "line": 2,
              "column": 5
            }
          }
        },
        "arguments": [],
        "optional": false,
        "loc": {
          "start": {
            "line": 2,
            "column": 1
          },
          "end": {
            "line": 2,
            "column": 7
          }
        }
      },
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 8
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 2,
      "column": 8
    }
  }
}
TypeError: Expected 1-2 arguments, but got 0 [functionCall_10.ts:2:1]
