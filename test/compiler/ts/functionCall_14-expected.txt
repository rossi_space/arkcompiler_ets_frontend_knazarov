{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "func",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSCallSignatureDeclaration",
                  "params": [
                    {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 18
                          },
                          "end": {
                            "line": 1,
                            "column": 24
                          }
                        }
                      },
                      "optional": true,
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 14
                        },
                        "end": {
                          "line": 1,
                          "column": 15
                        }
                      }
                    },
                    {
                      "type": "RestElement",
                      "argument": {
                        "type": "Identifier",
                        "name": "c",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 29
                          },
                          "end": {
                            "line": 1,
                            "column": 30
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 26
                        },
                        "end": {
                          "line": 1,
                          "column": 30
                        }
                      }
                    }
                  ],
                  "returnType": {
                    "type": "TSBooleanKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 33
                      },
                      "end": {
                        "line": 1,
                        "column": 40
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 13
                    },
                    "end": {
                      "line": 1,
                      "column": 41
                    }
                  }
                },
                {
                  "type": "TSCallSignatureDeclaration",
                  "params": [
                    {
                      "type": "Identifier",
                      "name": "a",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 43
                        },
                        "end": {
                          "line": 1,
                          "column": 44
                        }
                      }
                    },
                    {
                      "type": "Identifier",
                      "name": "b",
                      "typeAnnotation": {
                        "type": "TSArrayType",
                        "elementType": {
                          "type": "TSStringKeyword",
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 49
                            },
                            "end": {
                              "line": 1,
                              "column": 55
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 49
                          },
                          "end": {
                            "line": 1,
                            "column": 57
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 46
                        },
                        "end": {
                          "line": 1,
                          "column": 47
                        }
                      }
                    },
                    {
                      "type": "RestElement",
                      "argument": {
                        "type": "Identifier",
                        "name": "c",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 62
                          },
                          "end": {
                            "line": 1,
                            "column": 63
                          }
                        }
                      },
                      "typeAnnotation": {
                        "type": "TSArrayType",
                        "elementType": {
                          "type": "TSNumberKeyword",
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 65
                            },
                            "end": {
                              "line": 1,
                              "column": 71
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 65
                          },
                          "end": {
                            "line": 1,
                            "column": 73
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 59
                        },
                        "end": {
                          "line": 1,
                          "column": 73
                        }
                      }
                    }
                  ],
                  "returnType": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 76
                      },
                      "end": {
                        "line": 1,
                        "column": 82
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 42
                    },
                    "end": {
                      "line": 1,
                      "column": 83
                    }
                  }
                },
                {
                  "type": "TSCallSignatureDeclaration",
                  "params": [
                    {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 88
                          },
                          "end": {
                            "line": 1,
                            "column": 94
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 85
                        },
                        "end": {
                          "line": 1,
                          "column": 86
                        }
                      }
                    },
                    {
                      "type": "Identifier",
                      "name": "b",
                      "typeAnnotation": {
                        "type": "TSBooleanKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 99
                          },
                          "end": {
                            "line": 1,
                            "column": 106
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 96
                        },
                        "end": {
                          "line": 1,
                          "column": 97
                        }
                      }
                    }
                  ],
                  "returnType": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 109
                      },
                      "end": {
                        "line": 1,
                        "column": 115
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 84
                    },
                    "end": {
                      "line": 1,
                      "column": 117
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 11
                },
                "end": {
                  "line": 1,
                  "column": 117
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 9
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 9
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 118
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": {
            "type": "CallExpression",
            "callee": {
              "type": "Identifier",
              "name": "func",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 17
                },
                "end": {
                  "line": 2,
                  "column": 21
                }
              }
            },
            "arguments": [
              {
                "type": "NumberLiteral",
                "value": 1,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 22
                  },
                  "end": {
                    "line": 2,
                    "column": 23
                  }
                }
              }
            ],
            "optional": false,
            "loc": {
              "start": {
                "line": 2,
                "column": 17
              },
              "end": {
                "line": 2,
                "column": 24
              }
            }
          },
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 24
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 25
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 2,
      "column": 25
    }
  }
}
TypeError: Type 'boolean' is not assignable to type 'string' [functionCall_14.ts:2:5]
