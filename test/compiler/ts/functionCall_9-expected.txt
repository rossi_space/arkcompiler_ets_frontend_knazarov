{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 14
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 18
                },
                "end": {
                  "line": 1,
                  "column": 24
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 15
              },
              "end": {
                "line": 1,
                "column": 16
              }
            }
          },
          {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 29
                },
                "end": {
                  "line": 1,
                  "column": 35
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 26
              },
              "end": {
                "line": 1,
                "column": 27
              }
            }
          },
          {
            "type": "Identifier",
            "name": "c",
            "typeAnnotation": {
              "type": "TSArrayType",
              "elementType": {
                "type": "TSNumberKeyword",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 41
                  },
                  "end": {
                    "line": 1,
                    "column": 47
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 41
                },
                "end": {
                  "line": 1,
                  "column": 49
                }
              }
            },
            "optional": true,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 37
              },
              "end": {
                "line": 1,
                "column": 38
              }
            }
          }
        ],
        "body": {
          "type": "BlockStatement",
          "statements": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 51
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 1
            },
            "end": {
              "line": 5,
              "column": 5
            }
          }
        },
        "arguments": [
          {
            "type": "NumberLiteral",
            "value": 1,
            "loc": {
              "start": {
                "line": 5,
                "column": 6
              },
              "end": {
                "line": 5,
                "column": 7
              }
            }
          }
        ],
        "optional": false,
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 8
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 9
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 9
    }
  }
}
TypeError: Expected 2-3 arguments, but got 1 [functionCall_9.ts:5:1]
