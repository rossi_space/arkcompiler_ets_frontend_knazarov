{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSIndexSignature",
            "parameters": {
              "type": "Identifier",
              "name": "x",
              "typeAnnotation": {
                "type": "TSNumberKeyword",
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 9
                  },
                  "end": {
                    "line": 2,
                    "column": 15
                  }
                }
              },
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 6
                },
                "end": {
                  "line": 2,
                  "column": 7
                }
              }
            },
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 18
                },
                "end": {
                  "line": 2,
                  "column": 24
                }
              }
            },
            "readonly": false,
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 25
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "A",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSIndexSignature",
            "parameters": {
              "type": "Identifier",
              "name": "x",
              "typeAnnotation": {
                "type": "TSNumberKeyword",
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 9
                  },
                  "end": {
                    "line": 6,
                    "column": 15
                  }
                }
              },
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 6
                },
                "end": {
                  "line": 6,
                  "column": 7
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 6,
                  "column": 18
                },
                "end": {
                  "line": 6,
                  "column": 24
                }
              }
            },
            "readonly": false,
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 6,
                "column": 25
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 13
          },
          "end": {
            "line": 7,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "B",
        "decorators": [],
        "loc": {
          "start": {
            "line": 5,
            "column": 11
          },
          "end": {
            "line": 5,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 7,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "A",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 9,
                    "column": 8
                  },
                  "end": {
                    "line": 9,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 9,
                  "column": 8
                },
                "end": {
                  "line": 9,
                  "column": 9
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 9,
                "column": 5
              },
              "end": {
                "line": 9,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 9,
              "column": 5
            },
            "end": {
              "line": 9,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 9,
          "column": 1
        },
        "end": {
          "line": 9,
          "column": 10
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "B",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 10,
                    "column": 8
                  },
                  "end": {
                    "line": 10,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 10,
                  "column": 8
                },
                "end": {
                  "line": 10,
                  "column": 9
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 10,
                "column": 5
              },
              "end": {
                "line": 10,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 10,
              "column": 5
            },
            "end": {
              "line": 10,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 10,
          "column": 1
        },
        "end": {
          "line": 10,
          "column": 10
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 12,
              "column": 1
            },
            "end": {
              "line": 12,
              "column": 2
            }
          }
        },
        "right": {
          "type": "Identifier",
          "name": "b",
          "decorators": [],
          "loc": {
            "start": {
              "line": 12,
              "column": 5
            },
            "end": {
              "line": 12,
              "column": 6
            }
          }
        },
        "loc": {
          "start": {
            "line": 12,
            "column": 1
          },
          "end": {
            "line": 12,
            "column": 6
          }
        }
      },
      "loc": {
        "start": {
          "line": 12,
          "column": 1
        },
        "end": {
          "line": 12,
          "column": 7
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 12,
      "column": 7
    }
  }
}
TypeError: Type 'B' is not assignable to type 'A'. [interfaceAssignment8.ts:12:1]
