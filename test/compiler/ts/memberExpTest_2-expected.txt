{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 8
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 10
                },
                "end": {
                  "line": 2,
                  "column": 16
                }
              }
            },
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 17
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "a",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "foobar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 5
                },
                "end": {
                  "line": 6,
                  "column": 11
                }
              }
            },
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 13
                  },
                  "end": {
                    "line": 6,
                    "column": 14
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 13
                },
                "end": {
                  "line": 6,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 7,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 13
          },
          "end": {
            "line": 7,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "b",
        "decorators": [],
        "loc": {
          "start": {
            "line": 5,
            "column": 11
          },
          "end": {
            "line": 5,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 7,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "c",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "b",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 9,
                    "column": 8
                  },
                  "end": {
                    "line": 9,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 9,
                  "column": 8
                },
                "end": {
                  "line": 9,
                  "column": 9
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 9,
                "column": 5
              },
              "end": {
                "line": 9,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 9,
              "column": 5
            },
            "end": {
              "line": 9,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 9,
          "column": 1
        },
        "end": {
          "line": 9,
          "column": 10
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "d",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 10,
                    "column": 8
                  },
                  "end": {
                    "line": 10,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 10,
                  "column": 8
                },
                "end": {
                  "line": 10,
                  "column": 9
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 10,
                "column": 5
              },
              "end": {
                "line": 10,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 10,
              "column": 5
            },
            "end": {
              "line": 10,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 10,
          "column": 1
        },
        "end": {
          "line": 10,
          "column": 10
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "Identifier",
            "name": "c",
            "decorators": [],
            "loc": {
              "start": {
                "line": 11,
                "column": 1
              },
              "end": {
                "line": 11,
                "column": 2
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "foobar",
            "decorators": [],
            "loc": {
              "start": {
                "line": 11,
                "column": 3
              },
              "end": {
                "line": 11,
                "column": 9
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 11,
              "column": 1
            },
            "end": {
              "line": 11,
              "column": 9
            }
          }
        },
        "right": {
          "type": "Identifier",
          "name": "d",
          "decorators": [],
          "loc": {
            "start": {
              "line": 11,
              "column": 12
            },
            "end": {
              "line": 11,
              "column": 13
            }
          }
        },
        "loc": {
          "start": {
            "line": 11,
            "column": 1
          },
          "end": {
            "line": 11,
            "column": 13
          }
        }
      },
      "loc": {
        "start": {
          "line": 11,
          "column": 1
        },
        "end": {
          "line": 11,
          "column": 14
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "MemberExpression",
            "object": {
              "type": "Identifier",
              "name": "c",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 12,
                  "column": 1
                },
                "end": {
                  "line": 12,
                  "column": 2
                }
              }
            },
            "property": {
              "type": "Identifier",
              "name": "foobar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 12,
                  "column": 3
                },
                "end": {
                  "line": 12,
                  "column": 9
                }
              }
            },
            "computed": false,
            "optional": false,
            "loc": {
              "start": {
                "line": 12,
                "column": 1
              },
              "end": {
                "line": 12,
                "column": 9
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "foobar",
            "decorators": [],
            "loc": {
              "start": {
                "line": 12,
                "column": 10
              },
              "end": {
                "line": 12,
                "column": 16
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 12,
              "column": 1
            },
            "end": {
              "line": 12,
              "column": 16
            }
          }
        },
        "right": {
          "type": "Identifier",
          "name": "b",
          "decorators": [],
          "loc": {
            "start": {
              "line": 12,
              "column": 19
            },
            "end": {
              "line": 12,
              "column": 20
            }
          }
        },
        "loc": {
          "start": {
            "line": 12,
            "column": 1
          },
          "end": {
            "line": 12,
            "column": 20
          }
        }
      },
      "loc": {
        "start": {
          "line": 12,
          "column": 1
        },
        "end": {
          "line": 12,
          "column": 21
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 12,
      "column": 21
    }
  }
}
TypeError: Property 'foobar' does not exist on type 'a'. [memberExpTest_2.ts:12:10]
