{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "o",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 5
                    },
                    "end": {
                      "line": 2,
                      "column": 6
                    }
                  }
                },
                "value": {
                  "type": "NumberLiteral",
                  "value": 1,
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 8
                    },
                    "end": {
                      "line": 2,
                      "column": 9
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 5
                  },
                  "end": {
                    "line": 2,
                    "column": 9
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 9
              },
              "end": {
                "line": 3,
                "column": 2
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 5
              },
              "end": {
                "line": 5,
                "column": 6
              }
            }
          },
          "init": {
            "type": "TSAsExpression",
            "expression": {
              "type": "ObjectExpression",
              "properties": [
                {
                  "type": "SpreadElement",
                  "argument": {
                    "type": "Identifier",
                    "name": "o",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 13
                      },
                      "end": {
                        "line": 5,
                        "column": 14
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 10
                    },
                    "end": {
                      "line": 5,
                      "column": 14
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 5,
                  "column": 9
                },
                "end": {
                  "line": 5,
                  "column": 15
                }
              }
            },
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "const",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 19
                  },
                  "end": {
                    "line": 5,
                    "column": 24
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 5,
                  "column": 19
                },
                "end": {
                  "line": 5,
                  "column": 24
                }
              }
            },
            "loc": {
              "start": {
                "line": 5,
                "column": 9
              },
              "end": {
                "line": 5,
                "column": 25
              }
            }
          },
          "loc": {
            "start": {
              "line": 5,
              "column": 5
            },
            "end": {
              "line": 5,
              "column": 25
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 25
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 6,
                "column": 1
              },
              "end": {
                "line": 6,
                "column": 2
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 6,
                "column": 3
              },
              "end": {
                "line": 6,
                "column": 4
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 6,
              "column": 1
            },
            "end": {
              "line": 6,
              "column": 4
            }
          }
        },
        "right": {
          "type": "NumberLiteral",
          "value": 1,
          "loc": {
            "start": {
              "line": 6,
              "column": 7
            },
            "end": {
              "line": 6,
              "column": 8
            }
          }
        },
        "loc": {
          "start": {
            "line": 6,
            "column": 1
          },
          "end": {
            "line": 6,
            "column": 8
          }
        }
      },
      "loc": {
        "start": {
          "line": 6,
          "column": 1
        },
        "end": {
          "line": 6,
          "column": 9
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 6,
      "column": 9
    }
  }
}
TypeError: Cannot assign to 'a' because it is a read-only property. [member_expression_78.ts:6:3]
