{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "foo",
            "typeAnnotation": {
              "type": "TSConstructorType",
              "params": [
                {
                  "type": "Identifier",
                  "name": "a",
                  "typeAnnotation": {
                    "type": "TSAnyKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 18
                      },
                      "end": {
                        "line": 1,
                        "column": 21
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 15
                    },
                    "end": {
                      "line": 1,
                      "column": 16
                    }
                  }
                },
                {
                  "type": "Identifier",
                  "name": "b",
                  "typeAnnotation": {
                    "type": "TSAnyKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 26
                      },
                      "end": {
                        "line": 1,
                        "column": 29
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 23
                    },
                    "end": {
                      "line": 1,
                      "column": 24
                    }
                  }
                }
              ],
              "returnType": {
                "type": "TSAnyKeyword",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 34
                  },
                  "end": {
                    "line": 1,
                    "column": 37
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 37
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 8
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 8
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 38
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": {
            "type": "NewExpression",
            "callee": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 13
                },
                "end": {
                  "line": 2,
                  "column": 16
                }
              }
            },
            "arguments": [
              {
                "type": "StringLiteral",
                "value": "",
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 17
                  },
                  "end": {
                    "line": 2,
                    "column": 22
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 5,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 23
                  },
                  "end": {
                    "line": 2,
                    "column": 24
                  }
                }
              },
              {
                "type": "BooleanLiteral",
                "value": false,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 25
                  },
                  "end": {
                    "line": 2,
                    "column": 30
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 2,
                "column": 9
              },
              "end": {
                "line": 2,
                "column": 31
              }
            }
          },
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 31
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 32
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 3,
      "column": 1
    }
  }
}
TypeError: Expected 2 arguments, but got 3. [new_expression_157.ts:2:25]
