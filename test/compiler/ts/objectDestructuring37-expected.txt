{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 9
              },
              "end": {
                "line": 2,
                "column": 11
              }
            }
          },
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 11
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 12
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 8
                },
                "end": {
                  "line": 3,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 5
              },
              "end": {
                "line": 3,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 3,
              "column": 5
            },
            "end": {
              "line": 3,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 15
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "c",
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 4,
                  "column": 8
                },
                "end": {
                  "line": 4,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 4,
              "column": 5
            },
            "end": {
              "line": 4,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 4,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 15
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "ObjectPattern",
          "properties": [
            {
              "type": "Property",
              "method": false,
              "shorthand": false,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 3
                  },
                  "end": {
                    "line": 5,
                    "column": 4
                  }
                }
              },
              "value": {
                "type": "ObjectPattern",
                "properties": [
                  {
                    "type": "Property",
                    "method": false,
                    "shorthand": true,
                    "computed": false,
                    "key": {
                      "type": "Identifier",
                      "name": "c",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 8
                        },
                        "end": {
                          "line": 5,
                          "column": 9
                        }
                      }
                    },
                    "value": {
                      "type": "Identifier",
                      "name": "c",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 8
                        },
                        "end": {
                          "line": 5,
                          "column": 9
                        }
                      }
                    },
                    "kind": "init",
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 8
                      },
                      "end": {
                        "line": 5,
                        "column": 9
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 7
                  },
                  "end": {
                    "line": 5,
                    "column": 10
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 5,
                  "column": 3
                },
                "end": {
                  "line": 5,
                  "column": 10
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 5,
              "column": 2
            },
            "end": {
              "line": 5,
              "column": 11
            }
          }
        },
        "right": {
          "type": "ObjectExpression",
          "properties": [
            {
              "type": "Property",
              "method": false,
              "shorthand": false,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 15
                  },
                  "end": {
                    "line": 5,
                    "column": 16
                  }
                }
              },
              "value": {
                "type": "ObjectExpression",
                "properties": [
                  {
                    "type": "Property",
                    "method": false,
                    "shorthand": true,
                    "computed": false,
                    "key": {
                      "type": "Identifier",
                      "name": "b",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 20
                        },
                        "end": {
                          "line": 5,
                          "column": 21
                        }
                      }
                    },
                    "value": {
                      "type": "Identifier",
                      "name": "b",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 20
                        },
                        "end": {
                          "line": 5,
                          "column": 21
                        }
                      }
                    },
                    "kind": "init",
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 20
                      },
                      "end": {
                        "line": 5,
                        "column": 21
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 19
                  },
                  "end": {
                    "line": 5,
                    "column": 22
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 5,
                  "column": 15
                },
                "end": {
                  "line": 5,
                  "column": 22
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 5,
              "column": 14
            },
            "end": {
              "line": 5,
              "column": 23
            }
          }
        },
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 24
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 24
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 1
    }
  }
}
TypeError: Object literal may only specify known properties, and 'b' does not exist in type '{ c: string; }'. [objectDestructuring37.ts:5:20]
