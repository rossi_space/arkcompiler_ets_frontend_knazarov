{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "ObjectPattern",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": true,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 7
                    },
                    "end": {
                      "line": 1,
                      "column": 8
                    }
                  }
                },
                "value": {
                  "type": "AssignmentPattern",
                  "left": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 7
                      },
                      "end": {
                        "line": 1,
                        "column": 8
                      }
                    }
                  },
                  "right": {
                    "type": "ObjectExpression",
                    "properties": [
                      {
                        "type": "Property",
                        "method": false,
                        "shorthand": false,
                        "computed": false,
                        "key": {
                          "type": "Identifier",
                          "name": "a",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 13
                            },
                            "end": {
                              "line": 1,
                              "column": 14
                            }
                          }
                        },
                        "value": {
                          "type": "NumberLiteral",
                          "value": 5,
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 16
                            },
                            "end": {
                              "line": 1,
                              "column": 17
                            }
                          }
                        },
                        "kind": "init",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 13
                          },
                          "end": {
                            "line": 1,
                            "column": 17
                          }
                        }
                      },
                      {
                        "type": "Property",
                        "method": false,
                        "shorthand": false,
                        "computed": false,
                        "key": {
                          "type": "Identifier",
                          "name": "b",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 19
                            },
                            "end": {
                              "line": 1,
                              "column": 20
                            }
                          }
                        },
                        "value": {
                          "type": "BooleanLiteral",
                          "value": true,
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 22
                            },
                            "end": {
                              "line": 1,
                              "column": 26
                            }
                          }
                        },
                        "kind": "init",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 19
                          },
                          "end": {
                            "line": 1,
                            "column": 26
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 11
                      },
                      "end": {
                        "line": 1,
                        "column": 28
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 7
                    },
                    "end": {
                      "line": 1,
                      "column": 28
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 7
                  },
                  "end": {
                    "line": 1,
                    "column": 28
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 30
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 35
                    },
                    "end": {
                      "line": 1,
                      "column": 36
                    }
                  }
                },
                "value": {
                  "type": "BigIntLiteral",
                  "value": "",
                  "bigint": "",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 38
                    },
                    "end": {
                      "line": 1,
                      "column": 40
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 35
                  },
                  "end": {
                    "line": 1,
                    "column": 40
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 33
              },
              "end": {
                "line": 1,
                "column": 42
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 42
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 43
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 2,
              "column": 1
            },
            "end": {
              "line": 2,
              "column": 2
            }
          }
        },
        "right": {
          "type": "ObjectExpression",
          "properties": [
            {
              "type": "Property",
              "method": false,
              "shorthand": false,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 7
                  },
                  "end": {
                    "line": 2,
                    "column": 8
                  }
                }
              },
              "value": {
                "type": "NumberLiteral",
                "value": 1,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 10
                  },
                  "end": {
                    "line": 2,
                    "column": 11
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 7
                },
                "end": {
                  "line": 2,
                  "column": 11
                }
              }
            },
            {
              "type": "Property",
              "method": false,
              "shorthand": false,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "b",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 13
                  },
                  "end": {
                    "line": 2,
                    "column": 14
                  }
                }
              },
              "value": {
                "type": "BooleanLiteral",
                "value": false,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 16
                  },
                  "end": {
                    "line": 2,
                    "column": 21
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 13
                },
                "end": {
                  "line": 2,
                  "column": 21
                }
              }
            },
            {
              "type": "Property",
              "method": false,
              "shorthand": false,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "c",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 23
                  },
                  "end": {
                    "line": 2,
                    "column": 24
                  }
                }
              },
              "value": {
                "type": "NumberLiteral",
                "value": 6,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 26
                  },
                  "end": {
                    "line": 2,
                    "column": 27
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 23
                },
                "end": {
                  "line": 2,
                  "column": 27
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 29
            }
          }
        },
        "loc": {
          "start": {
            "line": 2,
            "column": 1
          },
          "end": {
            "line": 2,
            "column": 29
          }
        }
      },
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 30
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 2,
      "column": 30
    }
  }
}
TypeError: Object literal may only specify known properties, and 'c' does not exist in type '{ a: number; b: boolean; } | bigint' [objectDestructuring7.ts:2:23]
