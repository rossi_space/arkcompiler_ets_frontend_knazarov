{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 15
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "a",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 5
                },
                "end": {
                  "line": 6,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 6,
                  "column": 8
                },
                "end": {
                  "line": 6,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 6,
                "column": 15
              }
            }
          },
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 5
                },
                "end": {
                  "line": 7,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 7,
                  "column": 8
                },
                "end": {
                  "line": 7,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 7,
                "column": 5
              },
              "end": {
                "line": 7,
                "column": 15
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 13
          },
          "end": {
            "line": 8,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "b",
        "decorators": [],
        "loc": {
          "start": {
            "line": 5,
            "column": 11
          },
          "end": {
            "line": 5,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 8,
          "column": 2
        }
      }
    },
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 11,
                  "column": 5
                },
                "end": {
                  "line": 11,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 11,
                  "column": 8
                },
                "end": {
                  "line": 11,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 11,
                "column": 5
              },
              "end": {
                "line": 11,
                "column": 15
              }
            }
          },
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 12,
                  "column": 5
                },
                "end": {
                  "line": 12,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 12,
                  "column": 8
                },
                "end": {
                  "line": 12,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 12,
                "column": 5
              },
              "end": {
                "line": 12,
                "column": 15
              }
            }
          },
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "c",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 13,
                  "column": 5
                },
                "end": {
                  "line": 13,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSBooleanKeyword",
              "loc": {
                "start": {
                  "line": 13,
                  "column": 8
                },
                "end": {
                  "line": 13,
                  "column": 15
                }
              }
            },
            "loc": {
              "start": {
                "line": 13,
                "column": 5
              },
              "end": {
                "line": 14,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 10,
            "column": 13
          },
          "end": {
            "line": 14,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "c",
        "decorators": [],
        "loc": {
          "start": {
            "line": 10,
            "column": 11
          },
          "end": {
            "line": 10,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 10,
          "column": 1
        },
        "end": {
          "line": 14,
          "column": 2
        }
      }
    },
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "d",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 17,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 17,
                  "column": 8
                },
                "end": {
                  "line": 17,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 29
          },
          "end": {
            "line": 18,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "d",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 11
          },
          "end": {
            "line": 16,
            "column": 12
          }
        }
      },
      "extends": [
        {
          "type": "TSInterfaceHeritage",
          "expression": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 21
              },
              "end": {
                "line": 16,
                "column": 22
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 22
            },
            "end": {
              "line": 16,
              "column": 22
            }
          }
        },
        {
          "type": "TSInterfaceHeritage",
          "expression": {
            "type": "Identifier",
            "name": "b",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 24
              },
              "end": {
                "line": 16,
                "column": 25
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 25
            },
            "end": {
              "line": 16,
              "column": 25
            }
          }
        },
        {
          "type": "TSInterfaceHeritage",
          "expression": {
            "type": "Identifier",
            "name": "c",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 27
              },
              "end": {
                "line": 16,
                "column": 28
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 29
            },
            "end": {
              "line": 16,
              "column": 28
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 18,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "d",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 8
                  },
                  "end": {
                    "line": 20,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 20,
                  "column": 8
                },
                "end": {
                  "line": 20,
                  "column": 9
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 20,
                "column": 5
              },
              "end": {
                "line": 20,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 14
                    },
                    "end": {
                      "line": 20,
                      "column": 15
                    }
                  }
                },
                "value": {
                  "type": "NumberLiteral",
                  "value": 5,
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 17
                    },
                    "end": {
                      "line": 20,
                      "column": 18
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 14
                  },
                  "end": {
                    "line": 20,
                    "column": 18
                  }
                }
              },
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 20
                    },
                    "end": {
                      "line": 20,
                      "column": 21
                    }
                  }
                },
                "value": {
                  "type": "StringLiteral",
                  "value": "",
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 23
                    },
                    "end": {
                      "line": 20,
                      "column": 28
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 20
                  },
                  "end": {
                    "line": 20,
                    "column": 28
                  }
                }
              },
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "c",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 30
                    },
                    "end": {
                      "line": 20,
                      "column": 31
                    }
                  }
                },
                "value": {
                  "type": "BooleanLiteral",
                  "value": true,
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 33
                    },
                    "end": {
                      "line": 20,
                      "column": 37
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 30
                  },
                  "end": {
                    "line": 20,
                    "column": 37
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 20,
                "column": 12
              },
              "end": {
                "line": 20,
                "column": 39
              }
            }
          },
          "loc": {
            "start": {
              "line": 20,
              "column": 5
            },
            "end": {
              "line": 20,
              "column": 39
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 20,
          "column": 1
        },
        "end": {
          "line": 20,
          "column": 40
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 20,
      "column": 40
    }
  }
}
TypeError: Type '{ a: number; b: string; c: boolean; }' is not assignable to type 'd'. [objectLiteralAssignability16.ts:20:5]
