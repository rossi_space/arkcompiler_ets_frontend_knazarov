{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTupleType",
              "elementTypes": [
                {
                  "type": "TSNumberKeyword",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 9
                    },
                    "end": {
                      "line": 1,
                      "column": 16
                    }
                  }
                },
                {
                  "type": "TSStringKeyword",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 17
                    },
                    "end": {
                      "line": 1,
                      "column": 24
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 24
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 25
        }
      }
    },
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "b",
          "decorators": [],
          "loc": {
            "start": {
              "line": 3,
              "column": 10
            },
            "end": {
              "line": 3,
              "column": 11
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [],
        "returnType": {
          "type": "TSTupleType",
          "elementTypes": [
            {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 16
                },
                "end": {
                  "line": 3,
                  "column": 23
                }
              }
            },
            {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 24
                },
                "end": {
                  "line": 3,
                  "column": 31
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 3,
              "column": 15
            },
            "end": {
              "line": 3,
              "column": 31
            }
          }
        },
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "ArrayExpression",
                "elements": [
                  {
                    "type": "StringLiteral",
                    "value": "",
                    "loc": {
                      "start": {
                        "line": 4,
                        "column": 13
                      },
                      "end": {
                        "line": 4,
                        "column": 18
                      }
                    }
                  },
                  {
                    "type": "NumberLiteral",
                    "value": 5,
                    "loc": {
                      "start": {
                        "line": 4,
                        "column": 20
                      },
                      "end": {
                        "line": 4,
                        "column": 21
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 12
                  },
                  "end": {
                    "line": 4,
                    "column": 22
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 5
                },
                "end": {
                  "line": 4,
                  "column": 23
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 3,
              "column": 32
            },
            "end": {
              "line": 5,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 3,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 7,
              "column": 1
            },
            "end": {
              "line": 7,
              "column": 2
            }
          }
        },
        "right": {
          "type": "CallExpression",
          "callee": {
            "type": "Identifier",
            "name": "b",
            "decorators": [],
            "loc": {
              "start": {
                "line": 7,
                "column": 5
              },
              "end": {
                "line": 7,
                "column": 6
              }
            }
          },
          "arguments": [],
          "optional": false,
          "loc": {
            "start": {
              "line": 7,
              "column": 5
            },
            "end": {
              "line": 7,
              "column": 8
            }
          }
        },
        "loc": {
          "start": {
            "line": 7,
            "column": 1
          },
          "end": {
            "line": 7,
            "column": 8
          }
        }
      },
      "loc": {
        "start": {
          "line": 7,
          "column": 1
        },
        "end": {
          "line": 7,
          "column": 9
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 9
    }
  }
}
TypeError: Type '[string, number]' is not assignable to type '[number, string]'. [tupleAssignability9.ts:7:1]
