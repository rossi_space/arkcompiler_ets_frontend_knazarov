{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": true,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 9
                },
                "end": {
                  "line": 2,
                  "column": 15
                }
              }
            },
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 16
              }
            }
          },
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 5
                },
                "end": {
                  "line": 3,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSUnionType",
              "types": [
                {
                  "type": "TSStringKeyword",
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 8
                    },
                    "end": {
                      "line": 3,
                      "column": 14
                    }
                  }
                },
                {
                  "type": "TSBooleanKeyword",
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 17
                    },
                    "end": {
                      "line": 3,
                      "column": 24
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 8
                },
                "end": {
                  "line": 3,
                  "column": 24
                }
              }
            },
            "loc": {
              "start": {
                "line": 3,
                "column": 5
              },
              "end": {
                "line": 3,
                "column": 25
              }
            }
          },
          {
            "type": "TSMethodSignature",
            "computed": false,
            "optional": false,
            "key": {
              "type": "Identifier",
              "name": "c",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 5
                },
                "end": {
                  "line": 4,
                  "column": 6
                }
              }
            },
            "params": [
              {
                "type": "Identifier",
                "name": "a",
                "typeAnnotation": {
                  "type": "TSFunctionType",
                  "params": [
                    {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 14
                          },
                          "end": {
                            "line": 4,
                            "column": 20
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 4,
                          "column": 11
                        },
                        "end": {
                          "line": 4,
                          "column": 12
                        }
                      }
                    },
                    {
                      "type": "Identifier",
                      "name": "b",
                      "typeAnnotation": {
                        "type": "TSVoidKeyword",
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 25
                          },
                          "end": {
                            "line": 4,
                            "column": 29
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 4,
                          "column": 22
                        },
                        "end": {
                          "line": 4,
                          "column": 23
                        }
                      }
                    }
                  ],
                  "returnType": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 4,
                        "column": 34
                      },
                      "end": {
                        "line": 4,
                        "column": 40
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 10
                    },
                    "end": {
                      "line": 4,
                      "column": 40
                    }
                  }
                },
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 7
                  },
                  "end": {
                    "line": 4,
                    "column": 8
                  }
                }
              },
              {
                "type": "Identifier",
                "name": "b",
                "typeAnnotation": {
                  "type": "TSArrayType",
                  "elementType": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 4,
                        "column": 46
                      },
                      "end": {
                        "line": 4,
                        "column": 52
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 46
                    },
                    "end": {
                      "line": 4,
                      "column": 54
                    }
                  }
                },
                "optional": true,
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 42
                  },
                  "end": {
                    "line": 4,
                    "column": 43
                  }
                }
              }
            ],
            "typeAnnotation": {
              "type": "TSUndefinedKeyword",
              "loc": {
                "start": {
                  "line": 4,
                  "column": 57
                },
                "end": {
                  "line": 4,
                  "column": 66
                }
              }
            },
            "loc": {
              "start": {
                "line": 4,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 67
              }
            }
          },
          {
            "type": "TSCallSignatureDeclaration",
            "params": [
              {
                "type": "Identifier",
                "name": "a",
                "typeAnnotation": {
                  "type": "TSStringKeyword",
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 9
                    },
                    "end": {
                      "line": 5,
                      "column": 15
                    }
                  }
                },
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 6
                  },
                  "end": {
                    "line": 5,
                    "column": 7
                  }
                }
              }
            ],
            "returnType": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 5,
                  "column": 18
                },
                "end": {
                  "line": 5,
                  "column": 24
                }
              }
            },
            "loc": {
              "start": {
                "line": 5,
                "column": 5
              },
              "end": {
                "line": 5,
                "column": 25
              }
            }
          },
          {
            "type": "TSCallSignatureDeclaration",
            "params": [
              {
                "type": "Identifier",
                "name": "a",
                "typeAnnotation": {
                  "type": "TSNumberKeyword",
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 9
                    },
                    "end": {
                      "line": 6,
                      "column": 15
                    }
                  }
                },
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 6
                  },
                  "end": {
                    "line": 6,
                    "column": 7
                  }
                }
              }
            ],
            "returnType": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 6,
                  "column": 18
                },
                "end": {
                  "line": 6,
                  "column": 24
                }
              }
            },
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 6,
                "column": 25
              }
            }
          },
          {
            "type": "TSConstructSignatureDeclaration",
            "params": [
              {
                "type": "Identifier",
                "name": "a",
                "typeAnnotation": {
                  "type": "TSNullKeyword",
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 12
                    },
                    "end": {
                      "line": 7,
                      "column": 16
                    }
                  }
                },
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 9
                  },
                  "end": {
                    "line": 7,
                    "column": 10
                  }
                }
              },
              {
                "type": "Identifier",
                "name": "b",
                "typeAnnotation": {
                  "type": "TSStringKeyword",
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 22
                    },
                    "end": {
                      "line": 7,
                      "column": 28
                    }
                  }
                },
                "optional": true,
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 18
                  },
                  "end": {
                    "line": 7,
                    "column": 19
                  }
                }
              }
            ],
            "returnType": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 33
                      },
                      "end": {
                        "line": 7,
                        "column": 34
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 36
                      },
                      "end": {
                        "line": 7,
                        "column": 42
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 33
                    },
                    "end": {
                      "line": 7,
                      "column": 43
                    }
                  }
                },
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 44
                      },
                      "end": {
                        "line": 7,
                        "column": 45
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 47
                      },
                      "end": {
                        "line": 7,
                        "column": 53
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 44
                    },
                    "end": {
                      "line": 7,
                      "column": 54
                    }
                  }
                },
                {
                  "type": "TSMethodSignature",
                  "computed": false,
                  "optional": true,
                  "key": {
                    "type": "Identifier",
                    "name": "c",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 55
                      },
                      "end": {
                        "line": 7,
                        "column": 56
                      }
                    }
                  },
                  "params": [
                    {
                      "type": "ArrayPattern",
                      "elements": [
                        {
                          "type": "Identifier",
                          "name": "a",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 7,
                              "column": 59
                            },
                            "end": {
                              "line": 7,
                              "column": 60
                            }
                          }
                        },
                        {
                          "type": "Identifier",
                          "name": "b",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 7,
                              "column": 62
                            },
                            "end": {
                              "line": 7,
                              "column": 63
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 7,
                          "column": 58
                        },
                        "end": {
                          "line": 7,
                          "column": 64
                        }
                      }
                    }
                  ],
                  "typeAnnotation": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 67
                      },
                      "end": {
                        "line": 7,
                        "column": 73
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 55
                    },
                    "end": {
                      "line": 7,
                      "column": 75
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 31
                },
                "end": {
                  "line": 7,
                  "column": 75
                }
              }
            },
            "loc": {
              "start": {
                "line": 7,
                "column": 5
              },
              "end": {
                "line": 8,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 27
          },
          "end": {
            "line": 8,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "foo",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 14
          }
        }
      },
      "extends": [
        {
          "type": "TSInterfaceHeritage",
          "expression": {
            "type": "TSQualifiedName",
            "left": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 23
                },
                "end": {
                  "line": 1,
                  "column": 24
                }
              }
            },
            "right": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 25
                },
                "end": {
                  "line": 1,
                  "column": 26
                }
              }
            },
            "loc": {
              "start": {
                "line": 1,
                "column": 23
              },
              "end": {
                "line": 1,
                "column": 28
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 27
            },
            "end": {
              "line": 1,
              "column": 24
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 8,
          "column": 2
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 8,
      "column": 2
    }
  }
}
